// importing constants
const { PORT, URL } = require("./dependencies/constants/constants");

// external dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");

 // routing for user path
const user_router = require("./dependencies/routes/user");

 const movie_router = require("./dependencies/routes/movie");

 const app = express();
 app.use(cors());
 app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

 app.use(user_router);
app.use(movie_router);

mongoose.connect(URL, { useNewUrlParser: true }, () => {
  // connected to mongoose, ONLY THEN we listen to PORT
  app.listen(PORT, () => {
    console.log(`Server started running on ${PORT}!`);
  });
});
