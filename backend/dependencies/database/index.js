const User = require("./models/user");
const movie = require("./models/movie");

module.exports = { User, movie };
