const mongoose = require("mongoose");

const movieSchema = new mongoose.Schema({
  title: { type: String, required: true },
	genre: { type: String },
  director: { type: String}
});

const movie = mongoose.model("movie", movieSchema);
module.exports = movie;
