const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  phoneno:{ type: String, required: true,min:8 },
  nationality:{ type: String, required: true },
});

const User = mongoose.model("User", userSchema);
module.exports = User;
