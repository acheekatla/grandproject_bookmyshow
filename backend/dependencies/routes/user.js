//External imports
const express = require("express");
const router = express.Router();

// internal database dependencies
const User = require("../database/models/user");
const {
  validateEmail,
  validatePassword,
  validateUsername,
} = require("../validators");

/**
 * Route to respond to /register
 * Used to register a user to the database
 */
router.post("/register", (req, res) => {
  const { email, password, username,phoneno,nationality} = req.body;

  if (
    !validateEmail(email) ||
    !validatePassword(password) ||
    !validateUsername(username)
  ) {
    res.status(400).send({ err: "Validation fails" });
  } else {
    User.find({ email }, (err, data_array) => {
      if (err) {
        res.status(500).send({ err });
      } else {
        if (data_array.length != 0) {
          res
            .status(409)
            .send({ err: "user with email '" + email + "' already exists" });
        } else {
          const newUser = User({ email, password, username,phoneno,nationality});
          newUser.save((err, data) => {
            if (err) {
              res.status(500).send({ err });
            } else {
              const data_copy = JSON.parse(JSON.stringify(data));
              delete data_copy.__v;

              res.status(201).send({ data: data_copy });
            }
          });
        }
      }
    });
  }
});

/**
 * Route to respond to /login
 * Used to login a user to the application
 */
router.post("/login", (req, res) => {
  const { email, password } = req.body;
  console.log(email, password);

  if (!validateEmail(email) || !validatePassword(password)) {
    res.status(400).send({ err: "Validation fails" });
  } else {
    User.findOne({ email }, (eror, data) => {
      if (eror) {
        res.status(500).send({ err: eror });
      } else {
        if (data == null) {
          res.status(404).send({ err: "No use with this emaile exists." });
        } else {
          if (data.password !== password) {
            res
              .status(401)
              .send({ err: "Unauthorized, stupid password given." });
          } else {
            // LOGIN credentials were correct... soo.. in future, generate the JWT token
            const data_copy = JSON.parse(JSON.stringify(data));
            delete data_copy.__v;

            res.status(200).send({ data: data_copy });
          }
        }
      }
    });
  }
});

/**
 * Route to respond to /forgotpassword
 * Used to retrieve a password for a user
 */
router.post("/forgotpassword", (req, res) => {
  const { email, username } = req.body;
  console.log(email, username);

  if (!validateEmail(email) || !validateUsername(username)) {
    res.status(400).send({ err: "Validation fails" });
  } else {
    User.find({ username }, (err, data_array) => {
      if (err) {
        res.status(500).send({ err });
      } else {
        if (data_array.length == 0) {
          // no user with this username found!!
          res.status(401).send({
            err: "invalid username sent, not able to get your password",
          });
        } else {
          const data_obj = data_array[0];
          if (data_obj.email !== email) {
            res.status(401).send({ err: "username and email do not match" });
          } else {
            res.status(200).send({ data: { password: data_obj.password } });
          }
        }
      }
    });
  }
});

module.exports = router;
