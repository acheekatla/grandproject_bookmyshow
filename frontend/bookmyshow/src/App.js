import React from "react";
import Navbar from "./components/navbar/navbar";
import Login from "./components/login/login";
import Homepage from "./components/home/home";
import Register from "./components/register/register";

function App() {
  return (
   <>
   <Login/>
   <Homepage/>
   <Register/>
   <Navbar/>
   </>
  );
}

export default App;
