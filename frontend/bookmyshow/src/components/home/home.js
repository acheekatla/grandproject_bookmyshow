
/**
 *
 * @returns homepage
 */
function Homepage() {
  return (
    <>
      <h2>Home Page</h2>
      <p>Welcome back</p>
      <div className="container">
        <h1 className="upcomming">upcoming movies and shows</h1>
        <div className="item">
          <div className="item-right">
            <h2 className="num">23</h2>
            <p className="day">Oct</p>
            <span className="up-border"></span>
            <span className="down-border"></span>
          </div>
          {/* <!-- end item-right --> */}

          <div className="item-left">
            <p className="event">Music Event</p>
            <h2 className="title">Live In Mumbai</h2>
					<div className="sce">
              <div className="icon">
                <i className="fa fa-table"></i>
              </div>
              <p>
                Monday 15th September 2022 <br /> 15:20Pm & 11:00Am
              </p>
            </div>
            <div className="fix"></div>
            <div className="loc">
              <div className="icon">
                <i className="fa fa-map-marker"></i>
              </div>
              <p>
                North,South, India <br /> Party Number 16,20
              </p>
            </div>
            <div className="fix"></div>
            <button className="tickets">Tickets</button>
          </div>
          {/* <!-- end item-right --> */}
        </div>
        {/* <!-- end item --> */}

        <div className="item">
          <div className="item-right">
            <h2 className="num">23</h2>
            <p className="day">August</p>
            <span className="up-border"></span>
            <span className="down-border"></span>
          </div>
          {/* <!-- end item-right --> */}

          <div className="item-left">
            <p className="event">Music Event</p>
            <h2 className="title">Live Been Corrage</h2>

            <div className="sce">
              <div className="icon">
                <i className="fa fa-table"></i>
              </div>
              <p>
                Monday 18th 2022 <br /> 15:20Pm & 11:00Am
              </p>
            </div>
            <div className="fix"></div>
            <div className="loc">
              <div className="icon">
                <i className="fa fa-map-marker"></i>
              </div>
              <p>
                North,South, India <br /> Party Number 16,20
              </p>
            </div>
            <div className="fix"></div>
            <button className="tickets">Tickets</button>
          </div>
          {/* <!-- end item-right --> */}
        </div>
        {/* <!-- end item --> */}

        <div className="item">
          <div className="item-right">
            <h2 className="num">23</h2>
            <p className="day">September</p>
            <span className="up-border"></span>
            <span className="down-border"></span>
          </div>
          {/* <!-- end item-right --> */}

          <div className="item-left">
            <p className="event">Music Kaboom</p>
            <h2 className="title">Music Party</h2>

            <div className="sce">
              <div className="icon">
                <i className="fa fa-table"></i>
              </div>
              <p>
                Monday 25th 2022 <br /> 15:20Pm & 11:00Am
              </p>
            </div>
            <div className="fix"></div>
            <div className="loc">
              <div className="icon">
                <i className="fa fa-map-marker"></i>
              </div>
              <p>
                Hyderabad,India <br /> Party Number 16,20
              </p>
            </div>
            <div className="fix"></div>
            <button className="booked">Booked</button>
          </div>
        </div>

        <div className="item">
          <div className="item-right">
            <h2 className="num">26</h2>
            <p className="day">Nov</p>
            <span className="up-border"></span>
            <span className="down-border"></span>
          </div>

          <div className="item-left">
            <p className="event">Music Event</p>
            <h2 className="title linethrough">Hello India</h2>

            <div className="sce">
              <div className="icon">
                <i className="fa fa-table"></i>
              </div>
              <p>
                saturday 30th 2022 <br /> 15:20Pm & 11:00Am
              </p>
            </div>
            <div className="fix"></div>
            <div className="loc">
              <div className="icon">
                <i className="fa fa-map-marker"></i>
              </div>
              <p>
                India <br /> Party Number 16,20
              </p>
            </div>
            <div className="fix"></div>
            <button className="cancelled">cancelled</button>
          </div>
          {/* <!-- end item-right --> */}
        </div>
        {/* <!-- end item --> */}
      </div>
    </>
  );
}
// {% endblock %}
export default Homepage;
