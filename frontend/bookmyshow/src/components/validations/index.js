const validateUsername = (username_string) => {
  // write logic to check if valid username
  if (username_string != null && username_string.length != 0) {
    return true;
  } else {
    return false;
  }
};

const validateEmail = (email_string) => {
  // write logic to check if this is a valid email string, use regex
  if (email_string !== null && email_string.length !== 0) {
    return true;
  } else {
    return false;
  }
};

const validatePassword = (password_string) => {
  // write logic to check if this is a valid email string, use regex
  if (password_string !==  null && password_string.length !== 0) {
    return true;
  } else {
    return false;
  }
};

export { validateEmail, validatePassword, validateUsername };
