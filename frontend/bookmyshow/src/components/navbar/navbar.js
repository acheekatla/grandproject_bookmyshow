
function Navbar(){return(<>
  <body className="loggedin">
    <nav className="navtop">
      <div>
        <h1>BookMyShow</h1>
        <a href="{{ url_for('home') }}"><i className="fas fa-home"></i>Home</a>
        <a href="{{ url_for('profile') }}"><i className="fas fa-user-circle"></i>Profile</a>
        <a href="{{ url_for('logout') }}"><i className="fas fa-sign-out-alt"></i>Logout</a>
        <a href="{{ url_for('contactus') }}"><i className="fas fa-sign-out-alt"></i>contactus</a>
        <a href="{{ url_for('about') }}"><i className="fas fa-sign-out-alt"></i>about</a>
        <a href="{{ url_for('mytickets') }}"><i className="fas fa-sign-out-alt"></i>mytickets</a>
				<a href="{{ url_for('bookmyticket') }}"><i className="fas fa-sign-out-alt"></i>bookmyticket</a>
      </div>
    </nav>
    <div className="content">
      {/* {% block content %}
      {% endblock %} */}
    </div>
  </body>

  </>);}
  export default Navbar;
