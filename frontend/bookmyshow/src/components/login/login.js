
import { useState } from "react";
import { validateEmail, validatePassword } from "../validations/index";

	function Login() {
		  const [email, setEmail] = useState("");
		  const [password, setPassword] = useState("");
		
		  const onChangeHandler = (ev) => {
		    if (ev.target.name === "email") {
		      setEmail(ev.target.value);
		    } else if (ev.target.name === "password") {
		      setPassword(ev.target.value);
		    } else {
		      console.log(ev.target.name + " did not match with anyone");
		    }
		  };
		
		  const onClickHandler = (ev) => {
		    if (!validateEmail(email) || !validatePassword(password)) {
		      // invalid case
		    } else {
		      const data = { email, password };
		
		      fetch("http://localhost:3000/login", {
		        method: "POST",
		        headers: {
		          "Content-Type": "application/json",
		        },
		        body: JSON.stringify(data),
		      })
		        .then((response) => response.json())
		        .then((data) => {
		          console.log("Success:", data);
		        })
		        .catch((error) => {
		          console.error("Error:", error);
		        });
		    }
		  };
  return(
  <>
   <div className="login">
      <h1>BookMyShow</h1>
   <form action="/" method="post">
        <label for="username"></label>
        <input type="text" name="username" placeholder="Username" id="username" required />
        <label for="password"></label>
        <input type="password" name="password" placeholder="Password" id="password" required />
        <div className="msg"></div>
        <input type="submit" value="Login" />
      </form>
      </div>
  </>
  );
}
export default Login;